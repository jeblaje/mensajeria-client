// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  base_url: 'http://mensajeria.proyectostperg.xyz/server/public/api/',
  mapboxKey: 'pk.eyJ1IjoiamVibGFqZSIsImEiOiJjanVhOG05cGMwMDNrNDNwdWRhaWQ3cDZlIn0.9xalCJGchEiewO-ugazQ7Q',
  pusher: {
    key: '8f80b8ba0fc5b26b5ec5',
    cluster: 'us2',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
