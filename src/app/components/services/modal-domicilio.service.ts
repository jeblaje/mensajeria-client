import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModalDomicilioService {

  private _ocultarModal: boolean = true;
  public enviarDomi: EventEmitter<boolean> = new EventEmitter<boolean>();
  public domicilio: any = 'nuevo';

  get ocultarModal() {
    return this._ocultarModal;
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private ngZone: NgZone
  ) { }

  abrirModal(domicilio: any) {
    console.log({service: domicilio});
    this._ocultarModal = false;
    this.domicilio = domicilio;
  }

  cerrarModal() {
    this._ocultarModal = true;
  }

  cargarMisDomicilios() {
    const url = `/api/addresses`;
    let options = this.createRequestOptions('auth');
    return this.http.get(url, { headers: options });
  }

  cargarDimensiones() {
    const url = `/api/dimensions`;
    let options = this.createRequestOptions('auth');
    return this.http.get(url, { headers: options });
  }
  
  enviarMiDomicilio(domicilio) {
    const url = `/api/addresses`;
    let options = this.createRequestOptions('auth');
    return this.http.post(url, domicilio, { headers: options });
  }

  actualizarDomicilio(domicilio) {
    console.log({
      id: this.domicilio.id,
      data: domicilio
    });
    const url = `/api/addresses/${this.domicilio.id}`;
    let options = this.createRequestOptions('auth');
    return this.http.put(url, domicilio, { headers: options });
  }

  eliminarDomicilio(domicilio) {
    const url = `api/addresses/${domicilio.id}`;
    let options = this.createRequestOptions('auth');
    return this.http.delete(url, { headers: options });
  }

  cargarCiudades(id){
    const url = `/api/towns/${parseInt(id, 10)}`;
    let options = this.createRequestOptions('no-auth');
    return this.http.get(url, { headers: options })
      .pipe(
        map((resp) => {return resp;})
      );
  }

  cargarDepartamentos(){
    const url = `/api/departments`;
    let options = this.createRequestOptions('no-auth');
    return this.http.get(url, { headers: options });
  }



  buscarDireccion(direccion) {
      const url = `/api/location/search`;
      let options = this.createRequestOptions('auth');
      return this.http.post(url, direccion, { headers: options });
  }

  buscarDirecciones() {
    const url = `/api/locations`;
    let options = this.createRequestOptions('auth');
    return this.http.get(url, { headers: options });
}







  createRequestOptions(option) {

    let token = localStorage.getItem('token');
    let tokenTipe = localStorage.getItem('token-type');

    if (token == undefined || tokenTipe == undefined || token == null || tokenTipe == null) this.cleaner();

    let headersAuth = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `${tokenTipe} ${token}`
    });

    let headersNoAuth = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
    });
    return option === 'auth' ? headersAuth : headersNoAuth;
  }


  cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }


}
