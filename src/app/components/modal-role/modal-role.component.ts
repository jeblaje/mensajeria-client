import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from 'src/app/services/roles.service';
import { ModalDomicilioService } from '../services/modal-domicilio.service';

@Component({
  selector: 'app-modal-role',
  templateUrl: './modal-role.component.html',
  styles: [
  ]
})
export class ModalRoleComponent implements OnInit {


  public roleSubir: File;
  public imgTemp: any = null;

  
  roles: any[] = [];
  departamento: number = 20;
  piso: boolean = true;


  
  public roleForm = this.fb.group({
    permisions: [, Validators.required ],
  }, {
    // validators: this.passwordsIguales('pass1', 'pass2')
  });

    

  constructor(
    public roleService: RoleService,
    private toastr: ToastrService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    // this.cargarDepartamentos();
    // this.cargarCiudades(this.departamento);
  }

  pisoCheck(value) {
    if(value.target.checked) this.roleForm.controls.textPiso.disable();
    if(!value.target.checked) this.roleForm.controls.textPiso.enable();
  }


  cargarPermisos(rol) {
    this.roleService.cargarPermisos().subscribe(resp => {
      this.roles = resp.data;
      console.log('Permisos cargados.');
      // this.toastr.success(resp.message);
    });
  }

  // cargarCiudades(id) {
  //   this.roleService.cargarCiudades(id)
  //     .subscribe( (resp: any) => {
  //       this.ciudades = resp.towns;
  //     });
  // }

  // enviarMiDomicilio() {
  //   this.roleService.enviarMiDomicilio(this.roleForm.value)
  //     .subscribe( (resp: any) => {
  //       this.toastr.success(resp.message);
  //       this.cerrarModal();
  //     });
  // }


  cerrarModal() {
    this.roleService.cerrarModal();
  }














  // cambiarImagen(file: File) {
  //   this.imagenSubir = file;

  //   if (!file) {
  //     return this.imgTemp = null;
  //   }

  //   const reader = new FileReader();
  //   reader.readAsDataURL(file);

  //   reader.onloadend = () => {
  //     this.imgTemp = reader.result;
  //   }

  // }

  // subirImagen() {

  //   const id = this.modalImagenService.id;
  //   const tipo = this.modalImagenService.tipo;

  //   this.fileUploadService
  //     .actualizarFoto(this.imagenSubir, tipo, id)
  //     .then(img => {
  //       Swal.fire('Guardado', 'Imagen de usuario actualizada', 'success');

  //       this.modalImagenService.nuevaImagen.emit(img);

  //       this.cerrarModal();
  //     }).catch(err => {
  //       console.log(err);
  //       Swal.fire('Error', 'No se pudo subir la imagen', 'error');
  //     })

  // }

}
