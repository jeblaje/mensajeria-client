import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ModalDomicilioService } from '../services/modal-domicilio.service';
import * as Mapboxgl from 'mapbox-gl'
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-modal-domicilio',
  templateUrl: './modal-domicilio.component.html',
  styles: [
  ]
})
export class ModalDomicilioComponent implements OnInit {


  public domicilioSubir: File;
  public imgTemp: any = null;

  
  ciudades: any[] = [];
  departamentos: any[] = [];
  departamento: number = 20;
  piso: boolean = true;
  domicilio: string | any;
  mapa: Mapboxgl.Map;
  public location;
  public lat;
  public lng;
  public markerLocation;


  
  public domicilioForm = this.fb.group({
    nombre: ['Mi casa', Validators.required ],
    departamento: ['20', Validators.required ],
    ciudad: ['1042', [ Validators.required, Validators.email ] ],
    direccion: ['Calle 18c #12 - 25', Validators.required ],
    barrio: ['Primaveras', Validators.required ],
    piso: true ,
    textPiso: [ {value: '', disabled: true} ],
  }, {
    // validators: this.passwordsIguales('pass1', 'pass2')
  });

    

  constructor(
    public modalDomicilioService: ModalDomicilioService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    ) { 
    }
    
  ngOnInit(): void {
      this.cargarDepartamentos();
      this.cargarCiudades(this.departamento);
      this.domicilio =  this.modalDomicilioService.domicilio;
  }



  crearMarker(lng: number, lat: number) {

    var marker = new Mapboxgl.Marker({
      draggable: true
      })
      .setLngLat([lng, lat])
      .addTo(this.mapa);

      
    marker.on('dragend', () => {
       console.log(marker.getLngLat());
       this.markerLocation = marker.getLngLat();
    });
      
  }

  pisoCheck(value) {
    if(value.target.checked) this.domicilioForm.controls.textPiso.disable();
    if(!value.target.checked) this.domicilioForm.controls.textPiso.enable();
  }

  cargarDepartamentos() {
    this.modalDomicilioService.cargarDepartamentos()
      .subscribe( (resp: any) => {
        this.departamentos = resp.departmnets;
      });
  }

  cargarCiudades(id) {
    this.modalDomicilioService.cargarCiudades(id)
      .subscribe( (resp: any) => {
        this.ciudades = resp.towns;
      });
  }

  enviarMiDomicilio() {

    if(this.modalDomicilioService.domicilio != 'nuevo') {

      this.modalDomicilioService.actualizarDomicilio(this.domicilioForm.value)
      .subscribe( (resp: any) => {
        this.modalDomicilioService.enviarDomi.emit(true)
        this.toastr.success(resp.message);
        this.cerrarModal();
        console.log({Actualizando: resp});
      });
      
    } else {

      this.modalDomicilioService.enviarMiDomicilio(this.domicilioForm.value)
        .subscribe( (resp: any) => {
          this.modalDomicilioService.enviarDomi.emit(true)
          this.toastr.success(resp.message);
          this.cerrarModal();
        });

    }

  }

  editarrMiDomicilio() {
    this.modalDomicilioService.actualizarDomicilio(this.domicilioForm.value)
      .subscribe( (resp: any) => {
        this.modalDomicilioService.enviarDomi.emit(true)
        this.toastr.success(resp.message);
        this.cerrarModal();
      });
  }


  cerrarModal() {
    this.modalDomicilioService.cerrarModal();
  }














  // cambiarImagen(file: File) {
  //   this.imagenSubir = file;

  //   if (!file) {
  //     return this.imgTemp = null;
  //   }

  //   const reader = new FileReader();
  //   reader.readAsDataURL(file);

  //   reader.onloadend = () => {
  //     this.imgTemp = reader.result;
  //   }

  // }

  // subirImagen() {

  //   const id = this.modalImagenService.id;
  //   const tipo = this.modalImagenService.tipo;

  //   this.fileUploadService
  //     .actualizarFoto(this.imagenSubir, tipo, id)
  //     .then(img => {
  //       Swal.fire('Guardado', 'Imagen de usuario actualizada', 'success');

  //       this.modalImagenService.nuevaImagen.emit(img);

  //       this.cerrarModal();
  //     }).catch(err => {
  //       console.log(err);
  //       Swal.fire('Error', 'No se pudo subir la imagen', 'error');
  //     })

  // }

}
