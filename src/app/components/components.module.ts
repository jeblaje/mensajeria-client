import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { ChartsModule } from 'ng2-charts';

import { IncrementadorComponent } from './incrementador/incrementador.component';
import { DonaComponent } from './dona/dona.component';
import { ModalImagenComponent } from './modal-imagen/modal-imagen.component';
import { ModalDomicilioComponent } from './modal-domicilio/modal-domicilio.component';
import { ModalRoleComponent } from './modal-role/modal-role.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { UpdateComponent } from './update/update.component';



@NgModule({
  declarations: [
    IncrementadorComponent,
    DonaComponent,
    ModalImagenComponent,
    ModalDomicilioComponent,
    ModalRoleComponent,
    UpdateComponent
  ],
  exports: [
    IncrementadorComponent,
    DonaComponent,
    ModalImagenComponent,
    ModalDomicilioComponent,
    ModalRoleComponent,
    UpdateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ComponentsModule { }
