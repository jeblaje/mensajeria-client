import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ModalDomicilioService } from 'src/app/components/services/modal-domicilio.service';
import { RealTimeService } from 'src/app/services/real-time.service';
import { OrderService } from 'src/app/services/order.service';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { DeliveryService } from 'src/app/services/delivery.service';
declare function head_init();


@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})


export class PedidoComponent implements OnInit {

  // filial = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  totalDirecciones: number;
  selecedAddress: number;
  roleUserAuth = undefined;


  // public domicilioSubir: File;
  // public imgTemp: any = null;


  departments: any[] = [];
  towns: any[] = [];
  sizes: any[] = [];
  addresses: any[] = [];
  addresSearch: any[] = [];
  repartidores: any[] = [];
  preBuscador: boolean = false;
  keyword = "Calle 18c #12 - 25";
  public pathSeleceted;
  buscando: boolean = false;

  dname: string = '';
  sname: string = '';

  enviando: boolean = false;
  // addresSearch$: Observable<any[]>;



  selecedAddressID: number = null;
  department: number = 1;
  repartidor: number;
  town: number = 1;
  size: number = 1;
  declared_value: number = undefined;
  round_trip: boolean = false;

  dir: string  = undefined;
  nbh: string  = undefined;
  des: string  = undefined;
  positionDir: number = null;
  newPositionDir: number = null;
  formDirValid: boolean = undefined;

  formValid: boolean = true;



  constructor(
    public modalDomicilioService: ModalDomicilioService,
    private toastr: ToastrService,
    private orderService: OrderService,
    private repartidorService: DeliveryService,
  ) {
    this.cargarMisDomicilios();
    this.cargarDepartamentos();
    this.cargarDimensiones();
    this.cargarRepartidores();
    this.cargarCiudades(20);
    let user = JSON.parse(localStorage.getItem('user'));
    this.roleUserAuth = user.roles[0].id;
    // this.sizes.push(
    //   { id: 1, name: 'No requiere', description: 'Para tu servicio no necesitas ninguno en especial.', size: '', img: 'assets/images/icon/ic-ninguno.png' },
    //   { id: 2, name: 'Sobre', description: 'Si necesitas enviar documentos, esta es tu mejor opcion.', size: '(Carta u Oficio)', img: 'assets/images/icon/ic-sobre.png' },
    //   { id: 3, name: 'Maleta', description: 'Ideal para productos que no superan los 38 cms.', size: '(40x38x38 cms)', img: 'assets/images/icon/ic-maleta.png' },
    //   { id: 4, name: 'Baúl o maletero', description: 'Ideal para paquetes pequeños como una caja de zapatos.', size: '(30x30x30 cms)', img: 'assets/images/icon/ic-baul.png' },
    //   { id: 5, name: 'Parrilla', description: 'Ideal si necesitas llevar paquetes de hasta 50x50x50 cms.', size: '-(50x50x50 cms)', img: 'assets/images/icon/ic-parrilla.png' },
    // );
  }

  seleccionar(selecedDir) {
    this.pathSeleceted = selecedDir;
    this.dir = selecedDir.address;
    this.nbh = selecedDir.neighborhood;
    this.preBuscador = false;
    this.pathSeleceted = null;
    this.buscando = false;
    this.selecedAddressID = selecedDir.id
  }

  ngOnInit(): void {
    this.newPositionDir = this.addresses.length + 1;

    this.addresses.push(
      {   id: null, positionArray: 1, address: 'Calle 18c #36 - 44', neighborhood: 'Villa luz', description: 'Venir a recoger un bolso.' },
      {   id: null, positionArray: 2, address: 'MZ 4 - C 22', neighborhood: '450 Años', description: 'Donde debe llevar el bolso y traer un almuerzo.' },
    //  {   id: null, positionArray: 3, address: 'Av. brazil', neighborhood: 'Tercio', description: 'Llevar el bolso y traer el bolso de nuevo.' },
    );
  }


  // INICIALIZACIONES
  cargarDimensiones() {
    this.modalDomicilioService.cargarDimensiones()
      .subscribe((resp: any) => {
        this.sizes = resp.data;
      });

  }

  cargarRepartidores() {
    this.repartidorService.cargarRepartidores()
    .subscribe((resp: any) => {
        this.repartidores = resp.data;
    });
  }


  cargarMisDomicilios() {
    this.modalDomicilioService.cargarMisDomicilios()
      .subscribe((resp: any) => {
        this.totalDirecciones = resp.addresses.length
      });
  }

  cargarDepartamentos() {
    this.modalDomicilioService.cargarDepartamentos()
      .subscribe((resp: any) => {
        this.departments = resp.departmnets;
      });
  }

  cargarCiudades(id) {
    this.modalDomicilioService.cargarCiudades(id)
      .subscribe((resp: any) => {
        this.towns = resp.towns;
      });
  }



  // DIRECCIONES

  indice(address) {
    var indice = this.addresses.indexOf(address);
    return indice + 1;
  }

  agregarDir(dir, barrio, descripcion) {
    if (dir == '' || barrio == '' || descripcion == '' || dir == undefined || barrio == undefined || descripcion == undefined) {
      this.formDirValid = true;
      return;
    } else {
      this.formDirValid = false;
      var indice = this.addresses.length
      this.addresses.push({
        id: this.selecedAddressID, 
        positionArray: indice + 1, 
        address: dir, 
        neighborhood: barrio, 
        description: descripcion 
      });
      this.newPositionDir = this.addresses.length + 1,
      this.cancelar();
      // if (this.declared_value < 1000) this.formValid = false;
      // if (this.declared_value > 1000) this.formValid = true;

      // this.dir = undefined;
      // this.nbh = undefined;
      // this.des = undefined;
    }

    // if(this.addresses.length > 2) this.formValid = false;
  }

  eliminarDir(address) {
    // if (this.addresses.length < 3) return;
    this.addresses.splice(this.indice(address) - 1, 1);
    this.newPositionDir = this.addresses.length + 1;
  }

  editarDirGet(address) {
    this.dir = address.address;
    this.nbh = address.neighborhood;
    this.des = address.description;
    this.positionDir = address.positionArray - 1;
    this.selecedAddressID = address.id;
  }

  editarDir() {
    this.addresses[this.positionDir] = { id: this.selecedAddressID, positionArray: this.positionDir + 1, address: this.dir, neighborhood: this.nbh, description: this.des };
    this.newPositionDir = this.addresses.length + 1;
  }

  newAddressPosition() {
    var n: number = this.addresses.length + 1
    return n;
  }


  buscarDireccion(search) {
    this.buscando = true;
    let timeout;
    let data = {
      address: search.target.value,
      department_id: 20
    };
    
    this.modalDomicilioService.buscarDireccion(data)
    .subscribe((resp: any) => {
      this.addresSearch = resp.data;
      this.buscando = false;
    });

    // clearTimeout(timeout)
    // timeout = setTimeout(() => {
    //   console.log('Has dejado de escribir en el input');
      
      // if(search.target.value.length > 4) {
      // }

    //   clearTimeout(timeout)
    // },2000)
    
  }

  // buscarDirecciones() {
  //   // this.addresSearch$ =  this.modalDomicilioService.buscarDirecciones();
  //   // console.log(this.addresSearch$);
  //   this.modalDomicilioService.buscarDirecciones()
  //   .subscribe((resp: any) => {
  //     this.data = resp.data;
  //     console.log(resp.data);
  //   });
  // }


  cancelar() {
    this.selecedAddressID = null; 
    this.dir = undefined; 
    this.nbh = undefined; 
    this.des = undefined; 
    this.buscando = false; 
    this.preBuscador = false; 
    this.positionDir = null;
  }


  enviarPedido() {

    this.enviando = true;

    var pedido = {
      department:       this.department,
      town:             this.town,
      size:             this.size,
      status:           1,
      declared_value:   this.declared_value,
      round_trip:       this.round_trip == false ? 0 : 1,
      dirs:             this.addresses,
      delivery:         this.repartidor,
      dname: this.dname,
      sname: 'Order new',
    }

    console.log(pedido);
    
    if (this.valiData()) {
      this.orderService.guardarPedido(pedido).subscribe( (resp) => {
        console.log(resp);
        this.toastr.success(resp.message);
        this.selecedAddressID = null;
        this.declared_value = undefined;
        this.round_trip = false;
        this.dir = undefined;
        this.nbh = undefined;
        this.des = undefined;
        this.positionDir = undefined;
        this.formDirValid = undefined;
        this.formValid = true;
        this.enviando = false;
      });
    
    }
    
  }


  valiData() {
    let status = false;
    if (this.addresses.length > 1) {this.formValid = false;  status = true;} else {this.formValid = true; status = false; this.toastr.error('Deben haber 2 direcciones como minimo.');}
    if (this.declared_value > 1000) {this.formValid = false; status = true;} else {this.formValid = true; status = false; this.toastr.error('No a declarado el valor de su pedido.');}
    return status;
  }


  dnameF(id) {
    const size = this.sizes.find(element => element.id == id);
    this.dname = size.name;
  }


}