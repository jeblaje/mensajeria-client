import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';

import { Usuario } from '../../models/usuario.model';

import { BusquedasService } from '../../services/busquedas.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/services/roles.service';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/models/pedido.model';
import { RealTimeService } from 'src/app/services/real-time.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styles: [
  ]
})
export class PedidosComponent implements OnInit {

  public totalPedidos: number = 0;
  public totalPedidosCargados: number = 0;
  public pedidos: Order[] = [];
  public pedidosTemp: Order[] = [];
  roleUserAuth: number = undefined;

  public imgSubs: Subscription;
  public page: number = 1;
  public cargando: boolean = true;

  constructor(
    private orderService: OrderService,
    private router: Router,
    private roleService: RoleService,
    private toastr: ToastrService,
    private busquedasService: BusquedasService,
    private timeService: RealTimeService
  ) {
    let user = JSON.parse(localStorage.getItem('user'));
    this.roleUserAuth = user.roles[0].id;
   }

  ngOnDestroy(): void {
    // this.imgSubs.unsubscribe();
  }
  
  ngOnInit(): void {
    this.cargarPedidos();
    this.orderObserver();
    
    // this.imgSubs = this.modalImagenService.nuevaImagen
    //   .pipe(delay(100))
    //   .subscribe( img => this.cargarPedidos() );
  }


  orderObserver() {
    let event_name = 'StatusOrderEvent';
    this.timeService.channel.bind(event_name, data => this.pedidos.unshift(data.data));
  }

  cargarPedidos() {
    this.cargando = true;
    this.orderService.cargarPedidos(this.page)
      .subscribe(({ total, orders, totalResponse }) => {
        this.totalPedidos = total;
        this.pedidos = orders;
        this.pedidosTemp = orders;
        this.totalPedidosCargados = totalResponse;
        this.cargando = false;
        console.log(orders);
      }, (err) => {this.toastr.error('Al parecer tenemos un problema intenta mas tarde'); this.cargando = false;});
  }

  // cargarRoles() {
  //   this.roleService.cargarRoles().subscribe(resp => { this.roles = resp.data });
  // }

  cambiarPagina(valor: number,) {
    this.page += valor;

    if (this.page < 0) {
      this.page = 0;
    } else if (this.page >= this.totalPedidos) {
      this.page -= valor;
    }

    this.cargarPedidos();
  }

  buscar(termino: string) {

    if (termino.length === 0) {
      return this.pedidos = this.pedidosTemp;
    }

    // this.busquedasService.buscar('usuarios', termino)
    //   .subscribe((resp: Order[]) => {

    //     this.pedidos = resp;

    //   });
  }

  eliminarUsuario(usuario: Usuario) {
    if (usuario.id == localStorage.getItem('idLog')) {
      return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    }

    Swal.fire({
      title: '¿Borrar usuario?',
      text: `Esta a punto de borrar a ${usuario.name}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {

        // this.orderService.eliminarUsuario( usuario )
        //   .subscribe( resp => {

        //     this.cargarPedidos();
        //     Swal.fire(
        //       'Usuario borrado',
        //       `${ usuario.name } fue eliminado correctamente`,
        //       'success'
        //     );

        //   });

      }
    })

  }

  editarUsuario(usuario: Usuario) {
    // this.router.navigate(['/admin/usuarios/gestion', usuario.id]);
  }

  cambiarRole(usuario: Usuario) {

    // this.orderService.guardarUsuario( usuario )
    //   .subscribe( resp => {
    //     console.log(resp); 
    //   })
  }

  cambiarEstado() { }


  abrirModal(usuario: Usuario) {

    // this.modalImagenService.abrirModal('usuarios', usuario.id, usuario.img );
  }

  

}
