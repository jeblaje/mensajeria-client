import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';
import { delay } from 'rxjs/operators';

import { Registro } from '../../../models/registro.model';

import { BusquedasService } from '../../../services/busquedas.service';
import { ModalImagenService } from '../../../services/modal-imagen.service';
import { BitacoraService } from '../../../services/bitacora.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-bitacora',
  templateUrl: './bitacora.component.html',
  styles: [
  ]
})
export class BitacoraComponent implements OnInit {


  public totalRegistros: number = 0;
  public totalRegistrosCargados: number = 0;
  public registros: Registro[] = [];
  public registrosTemp: any[] = [];
  // public page: number[] = [];

  public imgSubs: Subscription;
  public page: number = 1;
  public cargando: boolean = true;

  constructor(
    private registroService: BitacoraService,
    private router: Router,
    private toastr: ToastrService,
    private busquedasService: BusquedasService,
    private modalImagenService: ModalImagenService
  ) { }

  ngOnDestroy(): void {
    // this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarRegistros();
  }


  cargarRegistros() {
    this.cargando = true;
    this.registroService.cargarRegistros(this.page)
      .subscribe(({ total, registros, totalResponse }) => {
        this.totalRegistros = total;
        this.registros = registros;
        this.registrosTemp = registros;
        this.totalRegistrosCargados = totalResponse;
        this.cargando = false;
        console.log(this.registros);

      });
  }



  private codeResponse(err) {

    if (err.status == 401 || err.status == 400) {
      this.toastr.error(err.error.message);
    }

    if (err.status == 500) {
      this.toastr.error('Estamos teniendo problemas disculpe');
    }

    if (err.status != 401 && err.status != 500 && err.status != 400) {
      this.toastr.error('Estamos teniendo problemas disculpe');
    }

  }


  cambiarPagina(valor: number,) {
    this.page += valor;

    if (this.page < 0) {
      this.page = 0;
    } else if (this.page >= this.totalRegistros) {
      this.page -= valor;
    }

    this.cargarRegistros();
  }

  buscar(termino: string) {

    // if ( termino.length === 0 ) {
    //   return this.registros = this.registrosTemp;
    // }

    // this.busquedasService.buscar( 'usuarios', termino )
    //     .subscribe( (resp: Registro[]) => {

    //       this.registros = resp;

    //     });
  }

  restaurarRegistro(registro: Registro) {
    // if ( registro.id == localStorage.getItem('idLog') ) {
    //   return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    // }

    Swal.fire({
      title: 'Restaurar registro?',
      text: 'Esta a punto de restaurar un registro eliminado',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, restauralo'
    }).then((result) => {
      if (result.value) {

        this.registroService.restaurarRegistro(registro)
          .subscribe(resp => {
            this.cargarRegistros();
            Swal.fire(
              'Registro restaurado',
              `${registro.id} fue restaurado correctamente`,
              'success'
            );
            this.router.navigate(['/dashboard/usuarios'])

          }, (err) => {
            this.codeResponse(err);
          })
      }
    })
  }

  cambiarRole(usuario: Registro) {

    // this.usuarioService.guardarUsuario( usuario )
    //   .subscribe( resp => {
    //     console.log(resp); 
    //   })
  }

}
