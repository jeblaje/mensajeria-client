import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from 'src/app/services/roles.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styles: [
  ]
})
export class RolesComponent implements OnInit {

  rolID: any = '';
  accion: any = '';
  roles: any[] = [];
  permissions: any[] = [];
  form: FormGroup;
  permissionsCheckedIDs: any[] = [];

  constructor(
    public toast: ToastrService,
    public roleService: RoleService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      role: ['', Validators.required],
      permisionsForm: new FormArray([])
    });
  }

  ngOnInit(): void {
    this.cargarRoles();
    this.cargarPermisos();
    this.addCheckboxes();
    this.checkNewRole();
  }



  get ordersFormArray() {
    return this.form.controls.permisionsForm as FormArray;
  }

  private addCheckboxes() {
    this.roleService.cargarPermisos().subscribe(resp => {
      resp.data.forEach(() => this.ordersFormArray.push(new FormControl(false)));
    });
  }

  submit() {
    var permissions = [];
    Object.keys(this.form.value.permisionsForm).map(v => {
      let idFormated = this.parseIntIdForm(v)
      if (idFormated != undefined) {
        permissions.push(idFormated);
      }
    });

    if (this.rolID != 'nuevo' && this.accion != 'nuevo') {
      this.roleService.actualizarRole(this.rolID, { name: this.form.value['role'], permissions }).subscribe(resp => {
        this.toast.info('El rol y sus permisos an sido actualizados!');
        this.cargarRoles();
        this.nuevoRol();
      });
    } else {
      this.roleService.crearRole(this.form.value['role'], permissions)
        .subscribe(resp => {
          this.cargarRoles();
          this.toast.success(resp.message);
        });

    }


  }

  nuevoRol() {
    this.rolID = 'nuevo';
    this.accion = 'nuevo';
    this.form.controls.role.setValue('');
    this.form.controls.role.enable();

    const formArray: FormArray = this.form.get('permisionsForm') as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      const e = formArray.controls[i];
      e.setValue(false);
    }
  }

  limpiarCheckbox() {
    const formArray: FormArray = this.form.get('permisionsForm') as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      const e = formArray.controls[i];
      e.setValue(false);
    }
  }

  checkNewRole() {
    let role = this.form.get('role').value;
    let search = this.roles.find(element => element.name.toLowerCase() == role.toLowerCase());
    const formArray: FormArray = this.form.get('permisionsForm') as FormArray;

    if (role == '') { this.form.controls.role.enable(); return };

    if (role == 'nuevo') {
      this.rolID = 'nuevo';
      this.form.controls.role.enable();
      const formArray: FormArray = this.form.get('permisionsForm') as FormArray;
      for (let i = 0; i < formArray.controls.length; i++) {
        const e = formArray.controls[i];
        e.setValue(false);
      }
      return;
    }

    if (role != '' || role != 'nuevo') {
      if (search != undefined) {
        this.toast.info('Cargando rol encontrado: ' + role);
        this.cargarRole(search);
        return;
      } else {
        if (this.accion != 'editar') {
          this.rolID = 'nuevo';
          // this.toast.success(role + 'Marca los permisos para guardar: ');
        }
        return;
      }
    }
  }

  cargarRole(rol) {
    this.rolID = rol.id;
    this.accion = 'editar';

    const formArray: FormArray = this.form.get('permisionsForm') as FormArray;
    this.roleService.cargarRole(rol.id).subscribe(resp => {
      this.form.controls.role.setValue(resp.data.role.name);
      const res = [];
      Object.keys(resp.data.permissions).map(v => {
        res.push(v);
      })
      for (let i = 0; i < formArray.controls.length; i++) {
        const e = formArray.controls[i];
        const u = i + 1;
        for (let o = 0; o < res.length; o++) {
          const a = res[o];
          if (u == a) {
            e.setValue(true);
            break;
          } else {
            e.setValue(false);
          }
        }
      }

      if(res == []) {
        this.limpiarCheckbox();
      }

      this.toast.success(resp.message);
    });
  }

  cargarRoles() {
    this.roleService.cargarRoles().subscribe(resp => {
      this.roles = resp.data;
      this.toast.info(resp.message);
    });
  }

  eliminarRole(rol: any) {
    // if ( rol.id == localStorage.getItem('idLog') ) {
    //   return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    // }

    Swal.fire({
      title: '¿Borrar rol?',
      text: `Esta a punto de borrar a ${rol.name}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo!'
    }).then((result) => {
      if (result.value) {

        this.roleService.eliminarRol(rol)
          .subscribe(resp => {

            this.cargarRoles();
            this.limpiarFormData();
            Swal.fire(
              'Rol borrado',
              `${rol.name} fue eliminado correctamente`,
              'success'
            );

          });

      }
    })

  }

  cargarPermisos() {
    this.roleService.cargarPermisos().subscribe(resp => {
      this.permissions = resp.data;
      this.toast.info(resp.message);
    });
  }

  check(event, permiso) {
    if (event.target.checked) this.permissionsCheckedIDs.push(permiso.id);
    if (!event.target.checked) this.removeItemFromArr(this.permissionsCheckedIDs, permiso.id);


    console.log({
      Event: event.target.checked,
      Permiso: permiso,
      Checkeados: this.permissionsCheckedIDs
    });
  }

  removeItemFromArr(arr, item) {
    return arr.filter(function (e) {
      return e !== item;
    });
  }



  abrirModal() {
    this.roleService.abrirModal();
    localStorage.getItem('user');
  }

  limpiarFormData() {
    const formArray: FormArray = this.form.get('permisionsForm') as FormArray;
    for (let i = 0; i < formArray.controls.length; i++) {
      const e = formArray.controls[i];
      e.setValue(false);
    }
  }

  private parseIntIdForm(v) {
    let i = parseInt(v, 10)
    let o = i + 1;
    if (this.form.value.permisionsForm[i]) return o;
  }

}
