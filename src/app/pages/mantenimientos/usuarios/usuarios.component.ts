import { Component, OnInit, OnDestroy } from '@angular/core';
import Swal from 'sweetalert2';
import { delay } from 'rxjs/operators';

import { Usuario } from '../../../models/usuario.model';

import { BusquedasService } from '../../../services/busquedas.service';
import { ModalImagenService } from '../../../services/modal-imagen.service';
import { UsuarioService } from '../../../services/usuario.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/services/roles.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: [
  ]
})
export class UsuariosComponent implements OnInit, OnDestroy {

  public totalUsuarios: number = 0;
  public totalUsuariosCargados: number = 0;
  public usuarios: Usuario[] = [];
  public usuariosTemp: Usuario[] = [];
  public roles: any[] = [];

  public imgSubs: Subscription;
  public page: number = 1;
  public cargando: boolean = true;

  constructor( private usuarioService: UsuarioService,
    private router: Router,
    private roleService: RoleService,
    private busquedasService: BusquedasService,
               private modalImagenService: ModalImagenService ) { }
               
  ngOnDestroy(): void {
    // this.imgSubs.unsubscribe();
  }

  ngOnInit(): void {
    this.cargarUsuarios();

    // this.imgSubs = this.modalImagenService.nuevaImagen
    //   .pipe(delay(100))
    //   .subscribe( img => this.cargarUsuarios() );
  }

  cargarUsuarios() {
    this.cargando = true;
    this.usuarioService.cargarUsuarios( this.page )
      .subscribe( ({ total, usuarios, totalResponse }) => {
        this.totalUsuarios = total;
        this.usuarios = usuarios;
        this.usuariosTemp = usuarios;
        this.totalUsuariosCargados = totalResponse;
        this.cargando = false;
    });
  }

  cargarRoles() {
    this.roleService.cargarRoles().subscribe(resp => {this.roles = resp.data});
  }

  cambiarPagina( valor: number, ) {
    this.page += valor;

    if ( this.page < 0 ) {
      this.page = 0;
    } else if ( this.page >= this.totalUsuarios ) {
      this.page -= valor; 
    }

    this.cargarUsuarios();
  }

  buscar( termino: string ) {

    if ( termino.length === 0 ) {
      return this.usuarios = this.usuariosTemp;
    }

    this.busquedasService.buscar( 'usuarios', termino )
        .subscribe( (resp: Usuario[]) => {

          this.usuarios = resp;

        });
  }

  eliminarUsuario( usuario: Usuario ) {
    if ( usuario.id == localStorage.getItem('idLog') ) {
      return Swal.fire('Error', 'No puede borrarse a si mismo', 'error');
    }

    Swal.fire({
      title: '¿Borrar usuario?',
      text: `Esta a punto de borrar a ${ usuario.name }`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {
        
        this.usuarioService.eliminarUsuario( usuario )
          .subscribe( resp => {
            
            this.cargarUsuarios();
            Swal.fire(
              'Usuario borrado',
              `${ usuario.name } fue eliminado correctamente`,
              'success'
            );
            
          });

      }
    })

  }

  editarUsuario(usuario: Usuario) {
    this.router.navigate(['/admin/usuarios/gestion', usuario.id]);
  }

  cambiarRole( usuario:Usuario ) {
    
    this.usuarioService.guardarUsuario( usuario )
      .subscribe( resp => {
        console.log(resp); 
      })
  }

  cambiarEstado() {}


  abrirModal( usuario: Usuario ) {
  
    // this.modalImagenService.abrirModal('usuarios', usuario.id, usuario.img );
  }

}
