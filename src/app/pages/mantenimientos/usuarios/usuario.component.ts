import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2'

import { ToastrService } from 'ngx-toastr';
import { UsuarioService } from 'src/app/services/usuario.service';
import { RoleService } from 'src/app/services/roles.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent {

  public formSubmitted = false;
  public roles = [];
  public paramValue = '';
  public gestion: string | number = 'Nuevo';

  public usuarioForm = this.fb.group({
    name: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    phone: ['', Validators.required],
    username: ['', Validators.required],
    document: ['', Validators.required],
    password: '',
    role: [3, Validators.required],
    state: ['Inactivo', Validators.required],

    // name: ['Fernando', Validators.required],
    // email: ['test100@gmail.com', [Validators.required, Validators.email]],
    // phone: ['3012083737', Validators.required],
    // username: ['fercho', Validators.required],
    // document: ['123456', Validators.required],
    // password: '123456',
    // role: [3, Validators.required],
    // state: ['Inactivo', Validators.required],
  }, {
    // validators: this.passwordsIguales('pass1', 'pass2')
  });

  constructor(
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private roleService: RoleService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.activatedRoute.params.subscribe( params => this.paramValue = params.id);
    this.cargarRoles();
      if (this.paramValue != 'nuevo') {this.gestion = 'Editando';  this.cargarUsuario(this.paramValue)};


    }



  crearUsuario() {
    // this.formSubmitted = true;
    console.log('Creando usuario...');

    console.log({Url: this.paramValue});
    console.log({Form: this.usuarioForm.value});

    if (this.usuarioForm.invalid) {
      return;
    }

    if (this.paramValue != 'nuevo') {

      this.usuarioForm.setControl('id', new FormControl(this.paramValue));
      this.usuarioService.actualizarUsuario(this.usuarioForm.value)
        .subscribe(resp => {
          this.toastr.success(resp.message);
          this.formSubmitted = false;
          this.router.navigate(['/admin']);
        }, (err) => this.errors(err));
        
      return;
    }

    this.usuarioService.crearUsuario(this.usuarioForm.value)
      .subscribe(resp => {
        this.toastr.success(resp.message);
    this.formSubmitted = false;
    this.router.navigate(['/admin']);
      }, (err) => this.errors(err));


  }

  cargarRoles() {
    this.roleService.cargarRoles().subscribe(resp => {this.roles = resp.data});
  }

  

  cargarUsuario(id) {
    this.usuarioService.cargarUsuario(id).subscribe(resp => {
      let roles = resp.data.roles;
      let rol;
      for (let i = 0; i < roles.length; i++) {
        if ( roles.length > 1) return;
        const element = roles[i];
        rol = element;
        
      }
        this.usuarioForm.controls.name.setValue(resp.data.name);
        this.usuarioForm.controls.email.setValue(resp.data.email);
        this.usuarioForm.controls.username.setValue(resp.data.username);
        this.usuarioForm.controls.phone.setValue(resp.data.phone);
        this.usuarioForm.controls.document.setValue(resp.data.document);
        this.usuarioForm.controls.password.setValue(resp.data.password);
        this.usuarioForm.controls.role.setValue(rol != undefined ? rol.id : 0);
        this.usuarioForm.controls.state.setValue(resp.data.state);
        console.log({Resp: resp});
    });
  }

  private errors(err) {
    console.log(err);
    if (err.status == 500) {
      let errorDuplicate = err.error.message.indexOf("SQLSTATE[23000]");
      if (errorDuplicate == 0) {
        this.toastr.error("El correo ya esta registrado!");
        return;
      }
      this.toastr.error("Intenta mas tarde al parecer tenemos problemas!");
      return;
    }

    if (err.status == 404) {
        this.toastr.error("Estamos teniendo problemas de acceso, intenta mas tarde!");
    }

    if (err.status == 403) {
      this.toastr.error(err.error.message);
  }

    this.formSubmitted = false;
    this.toastr.error("Error desconocido!");
  }


  campoNoValido(campo: string): boolean {

    if (this.usuarioForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }

  }

  contrasenasNoValidas() {
    const pass1 = this.usuarioForm.get('pass1').value;
    const pass2 = this.usuarioForm.get('pass2').value;

    if ((pass1 !== pass2) && this.formSubmitted) {
      return true;
    } else {
      return false;
    }

  }

  aceptaTerminos() {
    return !this.usuarioForm.get('terminos').value && this.formSubmitted;
  }

  passwordsIguales(pass1Name: string, pass2Name: string) {

    return (formGroup: FormGroup) => {

      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null)
      } else {
        pass2Control.setErrors({ noEsIgual: true })
      }


    }
  }

}
