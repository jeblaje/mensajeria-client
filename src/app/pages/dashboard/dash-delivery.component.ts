import { TmplAstRecursiveVisitor } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario.model';
import { SettingsService } from 'src/app/services/settings.service';
import { DeliveryService } from 'src/app/services/delivery.service';
import Swal from 'sweetalert2';
import { OrderService } from 'src/app/services/order.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dash-delivery',
  templateUrl: './dash-delivery.component.html',
  styleUrls: ['../../../assets/css/pages/inbox.css']
  // <link href="assets/css/pages/inbox.css" rel="stylesheet">
})
export class DashDeliveryComponent implements OnInit {

  public conectado: boolean | string;
  public user: Usuario;
  public totalPedidos: number;
  public section: number = 1;
  public pedidos: any = [];
  public equipamiento: any = {};

  public base: number = 100000;
  public moto: string;
  public bolso: string;
  public atuendo: string;
  public page: number = 1;

  public equipo: any[] = [];


  constructor(
    public settingsService: SettingsService,
    private orderService: OrderService,
    private toastr: ToastrService,
    public deliveryService: DeliveryService
  ) {
    let conected = localStorage.getItem('conectado');
    this.conectado = conected == 'equipando' ? 'equipando' : JSON.parse(conected) || false;
    this.user = JSON.parse(localStorage.getItem('user')) || null;
    // this.getEquipo();
    this.cargarPedidos();
  }

  ngOnInit(): void {
    // this.pedidos.push(
    //   {
    //     customer: 'John Doe',
    //     idCustomer: 1,
    //     // pick_up_point: 'Av. Simón Bolívar Calle 38',
    //     // delivery_point: 'Calle 18c #36 - 44',
    //     // address_id_: 'Simón Bolívar',
    //     // address_id: 'Villa luz',
    //     base_discount: 56000,
    //     pay: 3500,
    //     relaunching: 2500,
    //     round_trip: false,
    //     company: 'Farmatodo S.A.',
    //     delivery_type: 'Mensajeria',
    //     payment_method: 'Efectivo',
    //     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.',
    //     update_at: '5 minutes ago',
    //   },
    //   {
    //     customer: 'Marie Doe',
    //     idCustomer: 1,
    //     // pick_up_point: 'Av. Simón Bolívar Calle 38',
    //     // delivery_point: 'Calle 18c #36 - 44',
    //     // address_id_: 'Simón Bolívar',
    //     // address_id: 'Villa luz',
    //     base_discount: 56000,
    //     pay: 3500,
    //     relaunching: null,
    //     round_trip: false,
    //     company: 'Farmatodo S.A.',
    //     delivery_type: 'Domicilio',
    //     payment_method: 'PAGO EN LINEA',
    //     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.',
    //     update_at: 'Simón Boívar',
    //   },
    //   {
    //     customer: 'Soe Doe',
    //     idCustomer: 1,
    //     // pick_up_point: 'Av. Simón Bolívar Calle 38',
    //     // delivery_point: 'Calle 18c #36 - 44',
    //     // address_id_: 'Simón Bolívar',
    //     // address_id: 'Villa luz',
    //     base_discount: 56000,
    //     pay: 3500,
    //     relaunching: null,
    //     round_trip: false,
    //     company: 'Farmatodo S.A.',
    //     delivery_type: 'Mensajeria',
    //     payment_method: 'EFECTIVO',
    //     description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam.',
    //     update_at: 'Simón Boívar',
    //   }
    // );
    this.totalPedidos = this.pedidos.length;
  }     



  
  cargarPedidos() {
    // this.cargando = true;
    this.orderService.cargarServicios()
      .subscribe( (resp: any) => {
        // console.log(resp);
        // this.totalPedidos = total;
        this.pedidos = resp.data;
        // this.pedidosTemp = orders;
        console.log(this.pedidos);
        // this.totalPedidosCargados = totalResponse;
        // this.cargando = false;
      }, (err) => 
      {
        console.log(err);
        if( err.error.message == 'This action is unauthorized.') return this.toastr.error('No esta autorizado para esto!!');
        return this.toastr.error(err.error.message);
        
      }
      );
  }
  
  takeOrder(order, msg?) {
    Swal.fire({
      title: msg || 'Seguro?',
      text: 'Detalles!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, vamos!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Aceptado!',
          'Has tomado el pedido!',
          'success'
        )

        this.section = 11;
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'Busquemos otro :)',
          'error'
        )
      }
    })
  }

  takeNoOrder(order) {

    Swal.fire({
      title: 'Seguro?',
      text: 'Detalles!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, rechazalo!',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
          'Listo!',
          'El pedido a sido rechazado!',
          'success'
        )

      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        this.takeOrder(order, 'Deseas tomarlo?');
        // Swal.fire(
        //   'Cancelado',
        //   'Ok tomemoslo :)',
        //   'error'
        // )
      }
    })
  }



  OnOff() {
    let conected = localStorage.getItem('conectado');
    // OPCION SIGUIENTE NIVEL
    // let stateDEV = conected == 'equipando' ? 'equipando' : JSON.parse(conected);

    if (this.conectado) 
    { 
     
      this.deliveryService.deliveryOutService().subscribe( (resp:any) => {
        
        localStorage.setItem('conectado', 'false'); 
        this.conectado = false; 
        this.settingsService.changeThemeUrl(localStorage.getItem('theme-user')); 
        // localStorage.setItem('theme-user', localStorage.getItem('theme')); 
        // localStorage.setItem('conectado', 'equipando'); this.settingsService.changeTheme('blue') ; 

      });
      return; 
    };
    
    if (!this.conectado) { 

      this.deliveryService.deliveryOnService().subscribe( (resp:any) => {

        localStorage.setItem('conectado', 'true'); 
        this.conectado = true; 
        localStorage.setItem('theme-user', localStorage.getItem('theme')); 
        
      });
      return; 

      // OPCION SIGUIENTE NIVEL
      // this.conectado = 'equipando'; 
    };

  }

  deliveryIsActive(){
    this.deliveryService.deliveryOutService().subscribe( (resp:any) => {
        
      localStorage.setItem('conectado', 'false'); 
      this.conectado = false; 
      this.settingsService.changeThemeUrl(localStorage.getItem('theme-user')); 
      // localStorage.setItem('theme-user', localStorage.getItem('theme')); 
      // localStorage.setItem('conectado', 'equipando'); this.settingsService.changeTheme('blue') ; 

    });
  }







  

  // // INIT
  // getEquipo() {
  //   console.log('cargando kits');
  //   this.deliveryService.cargarKits({data: [1,2,3]}).subscribe( (resp:any) => {
      
  //     // console.log(resp);
  //     this.equipo = resp.data;

  //   });
  // }





  // FUNCIONES DEL SIGUIENTE NIVEL
  // continuar() {
    

  //   let equipo = {
  //     moto: this.moto,
  //     bolso: this.bolso,
  //     atuendo: this.atuendo,
  //     base: this.base,
  //   }
  //   console.log(equipo);

  //   this.deliveryService.envioEquipamiento(equipo).subscribe( (resp: any) => {
  //     this.conectado = true; 
  //     localStorage.setItem('conectado', 'true'); 
  //     this.settingsService.changeTheme('green');  

  //     console.log(resp);
  //   });


  // }

  // cancelar() {
  //     this.conectado = false; 
  //     localStorage.setItem('conectado', 'false'); 
  //     this.settingsService.changeThemeUrl(localStorage.getItem('theme-user')) ; return;
  // }



}
