import { Component, OnInit } from '@angular/core';
import { PedidosService } from 'src/app/services/pedidos.service';

@Component({
  selector: 'app-dash-admin',
  templateUrl: './dash-admin.component.html',
  styles: [
  ],
  // providers: [Pedidos]
})
export class DashAdminComponent implements OnInit {

  totalPedidos: number = 0;

  constructor(
    private pedidosService: PedidosService,
  ) { 
    this.totalPedidos = this.pedidosService.total;
  }

  ngOnInit(): void {
    // this.cargarPedidos();
  }

  
  // cargarPedidos() {
  //   this.pedidosService.cargarUsuarios().subscribe({

  //   });
  // }

}
