import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modulos
import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgxCurrencyModule, CurrencyMaskInputMode } from "ngx-currency";


import { NgSelectModule } from '@ng-select/ng-select';
import { PipesModule } from '../pipes/pipes.module';



// COMPONENTES
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashAdminComponent } from './dashboard/dash-admin.component';
import { DashDeliveryComponent } from './dashboard/dash-delivery.component';
import { PagesComponent } from './pages.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PerfilComponent } from './perfil/perfil.component';
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';
import { RolesComponent } from './mantenimientos/roles/roles.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidoComponent } from './pedidos/pedido.component';
import { FavoritosComponent } from './favoritos/favoritos.component';
import { BitacoraComponent } from './mantenimientos/bitacora/bitacora.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { UsuarioComponent } from './mantenimientos/usuarios/usuario.component';
import { ProductosComponent } from './mantenimientos/productos/productos.component';
import { DireccionesComponent } from './direcciones/direcciones.component';
import { AliadosComponent } from './aliados/aliados.component';
import { MensajeriasComponent } from './mensajerias/mensajerias.component';


export const customCurrencyMaskConfig = {
  align: "right",
  allowNegative: true,
  allowZero: true,
  decimal: ",",
  precision: 0,
  prefix: "$ ",
  suffix: "",
  thousands: ".",
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.FINANCIAL
};


@NgModule({
  providers: [
    CurrencyPipe
  ],
  declarations: [
    DashboardComponent,
    DashDeliveryComponent,
    DashAdminComponent,
    PagesComponent,
    AccountSettingsComponent,
    PerfilComponent,
    UsuariosComponent,
    UsuarioComponent,
    BusquedaComponent,
    BitacoraComponent,
    ProductosComponent,
    PedidosComponent,
    RolesComponent,
    DireccionesComponent,
    AliadosComponent,
    MensajeriasComponent,
    PedidosComponent,
    FavoritosComponent,
    PedidoComponent
  ],
  exports: [
    DashboardComponent,
    PagesComponent,
    NgxCurrencyModule,
    AccountSettingsComponent
  ],
  imports: [ 
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    SharedModule,
    RouterModule,
    ComponentsModule,
    PipesModule,
    AutocompleteLibModule,
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig)
  ]
})
export class PagesModule { }

