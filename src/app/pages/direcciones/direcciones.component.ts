import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ModalDomicilioService } from 'src/app/components/services/modal-domicilio.service';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';
import * as Mapboxgl from 'mapbox-gl'
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-direcciones',
  templateUrl: './direcciones.component.html',
  styles: [
  ]
})
export class DireccionesComponent implements OnInit, OnDestroy {

  public usuario: Usuario;
  public domicilio: any = 0;
  public domicilios: any[] = [];
  public domisilioSub: Subscription;
  public mapa: Mapboxgl.Map;
  public location = false;
  // public lat;
  // public lng;
  // public markerLocation;
  public pathSeleceted;
  public marker = null;
  // public markers = [];

  constructor(
    private usuarioService: UsuarioService,
    private toastr: ToastrService,
    public modalDomicilioService: ModalDomicilioService,
    private router: Router
  ) {
    this.cargarMisDomicilios();
    this.usuario = usuarioService.usuario;
    this.domicilio = localStorage.getItem('domi') || 'No hay direcciones';
  }

  ngOnInit(): void {
    this.domisilioSub = this.modalDomicilioService.enviarDomi.subscribe( () => this.cargarMisDomicilios());

    (Mapboxgl as any).accessToken = environment.mapboxKey;

    navigator.geolocation.getCurrentPosition((resp) => {
      this.mapa = new Mapboxgl.Map({
        container: 'mapa', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [resp.coords.longitude, resp.coords.latitude], // starting position
        zoom: 13 // starting zoom
      });
    });


  }

  ngOnDestroy() {
    this.domisilioSub.unsubscribe();
  }

  newMarker() {
    this.location = true;
    navigator.geolocation.getCurrentPosition((resp) => {
      this.mapa = new Mapboxgl.Map({
        container: 'mapa', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [resp.coords.longitude, resp.coords.latitude], // starting position
        zoom: 13 // starting zoom
      });
      this.crearMarker(resp.coords.longitude, resp.coords.latitude, 'new');
    });

  }

  crearMarker(lng: number, lat: number, action: string) {
    this.marker = null;

    let markNew = document.createElement('div');
    let options = this.location == true ? { draggable: true } : { draggable: false };
    if(action == 'new') markNew.className = 'marker';
    if(action == 'old') markNew.className = 'marker2';
    


    console.log(this.marker);
    console.log(markNew);
    console.log(this.location);

    

    if (this.location) {

        this.marker = new Mapboxgl.Marker(markNew, options)
          .setLngLat([lng, lat])
          .addTo(this.mapa);
        this.mapa.setCenter([lng, lat]);

    } else {
      this.marker.remove();
      this.marker = null;
    }


  }


  obtenerMiUbicacionActual() {
    navigator.geolocation.getCurrentPosition((resp) => {
      this.crearMarker(resp.coords.longitude, resp.coords.latitude, 'actual');
    }
    );
  }


  validarDirMap(domi) {
    console.log(domi);
  }

  domiMap(path) {
    this.pathSeleceted = path;
    // -73.25207049492995, 10.466867368869384
    this.crearMarker(parseFloat(path.lng), parseFloat(path.lat), 'old');
  }




  cambiarDomicilio(value: string) {
    console.log(value);
    localStorage.setItem('domi', value);
  }

  cargarMisDomicilios() {
    this.modalDomicilioService.cargarMisDomicilios()
      .subscribe((resp: any) => { console.log(resp); this.domicilios = resp.addresses });
    // console.log(this.domicilios);
  }

  editar(domicilio) {
    this.modalDomicilioService.abrirModal(domicilio);
  }

  eliminar(domicilio) {

    Swal.fire({
      title: '¿Borrar domicilio?',
      text: `Esta a punto de borrar el domicilio ${domicilio.name}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo'
    }).then((result) => {
      if (result.value) {

        this.modalDomicilioService.eliminarDomicilio(domicilio)
          .subscribe((resp) => {

            this.cargarMisDomicilios();
            console.log(resp);
            Swal.fire(
              'Domicilio borrado',
              `${domicilio.name} fue eliminado correctamente`,
              'success'
            );

          });

      }
    })
  }

  abrirModal(domicilio) {
    this.modalDomicilioService.abrirModal(domicilio);
    localStorage.getItem('user');
  }

}
