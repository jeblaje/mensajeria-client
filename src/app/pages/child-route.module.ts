import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PerfilComponent } from './perfil/perfil.component';

// Mantenimientos
import { UsuarioComponent } from './mantenimientos/usuarios/usuario.component';
import { UsuariosComponent } from './mantenimientos/usuarios/usuarios.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { BitacoraComponent } from './mantenimientos/bitacora/bitacora.component';
import { RolesComponent } from './mantenimientos/roles/roles.component';
import { MensajeriasComponent } from './mensajerias/mensajerias.component';
import { DireccionesComponent } from './direcciones/direcciones.component';
import { FavoritosComponent } from './favoritos/favoritos.component';
import { DashDeliveryComponent } from './dashboard/dash-delivery.component';
import { DashAdminComponent } from './dashboard/dash-admin.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidoComponent } from './pedidos/pedido.component';


const childRoutes: Routes = [

  // USUARIO
  { path: 'direcciones', component: DireccionesComponent, data: { titulo: 'Mis direcciones' } },
  { path: 'mensajerias', component: MensajeriasComponent, data: { titulo: 'Mensajerias' } },
  { path: 'favoritos', component: FavoritosComponent, data: { titulo: 'Favoritos' } },
  { path: 'perfil', component: PerfilComponent, data: { titulo: 'Pedidos' } },
  { path: 'estilos', component: AccountSettingsComponent, data: { titulo: 'Pedidos' } },
  { path: 'pedidos', component: PedidosComponent, data: { titulo: 'Mis pedidos' } },
  { path: 'pedidos/gestion/:id', component: PedidoComponent, data: { titulo: 'Pedido' } },
  


  // Rutas de inicio de pantala
  {
    path: 'dash-cli', children: [
      { path: '', component: DashboardComponent, data: { titulo: 'Inicio Cliente' } },
      { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil' } },
      { path: 'buscar/:termino', component: BusquedaComponent, data: { titulo: 'Busquedas' } },
    ]
  },

  {
    path: 'dash-dev', children: [
      { path: '', component: DashDeliveryComponent, data: { titulo: 'Inicio Repartidor' } },
      { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil' } },
      { path: 'buscar/:termino', component: BusquedaComponent, data: { titulo: 'Busquedas' } },
    ]
  },

  {
    path: 'dash-adm', children: [
      { path: '', component: DashAdminComponent, data: { titulo: 'Inicio administradors' } },
      { path: 'perfil', component: PerfilComponent, data: { titulo: 'Perfil' } },
      { path: 'buscar/:termino', component: BusquedaComponent, data: { titulo: 'Busquedas' } },
    ]
  },


  // Mantenimientos -- ADMIN
  {
    path: 'admin', children:
      [
        { path: '', component: UsuariosComponent, data: { titulo: 'Usuarios' } },
        { path: 'roles', component: RolesComponent, data: { titulo: 'Roles' } },
        { path: 'logs', component: BitacoraComponent, data: { titulo: 'Registros' } },
        { path: 'usuarios/gestion/:id', component: UsuarioComponent, data: { titulo: 'Gestion usuarios' } },
        { path: 'mensajerias/gestion/:id', component: MensajeriasComponent, data: { titulo: 'Gestion mensajerias' } },
      ]
  },


  // { path: '', redirectTo: 'inicio'},
  // { path: '**', redirectTo: 'no-found'},
  // { path: 'no-found', component: NopagefounsdComponent, data: { titulo: 'Pagina no encontrada' }},
  // { path: 'usuarios', canActivate: [ AdminGuard ], component: UsuariosComponent, data: { titulo: 'Matenimiento de Usuarios' }},
]



@NgModule({
  imports: [RouterModule.forChild(childRoutes)],
  exports: [RouterModule]
})
export class ChildRoutesModule { }
