  import { Injectable } from '@angular/core';
  import { HttpClient, HttpHeaders } from '@angular/common/http';
  import { Router } from '@angular/router';
  import { AppService } from './app.service';
  import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  private _ocultarModal = true;

  constructor(
    private http: HttpClient, 
    private router: Router,
    private appService: AppService,
  ) { }

  get ocultarModal() {
    return this._ocultarModal;
  }
  
  abrirModal() {
    this._ocultarModal = false;
  }

  cerrarModal() {
    this._ocultarModal = true;
  }


  cargarRoles() {
  
    const url = `/api/roles`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get(url, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }

  cargarRole(id) {
  
    const url = `/api/roles/${id}/edit`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get(url, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }

  actualizarRole(id, data) {
    const url = `/api/roles/${id}`;
    let dataFormat = {
      name: data.name,
      permissions: data.permissions
    }
    let options = this.appService.createRequestOptions('auth');
    return this.http.put(url, data, { headers: options })
    .pipe(
      map( (resp: any) => {
        console.log(resp);
        return resp;
      } )
    );
  }

  crearRole(role, permissions) {
    console.log({role, permissions});
    const url = `/api/roles`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.post(url, {name:role, permissions}, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }

  eliminarRol(role) {
    const url = `/api/roles/${role.id}`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.delete(url, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }


  cargarPermisos() {
  
    const url = `/api/roles/create`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get(url, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }


  cargarPermisosOfRol(rol) {
  
    const url = `/api/permisos/${rol.id}`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get(url, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      } )
    );
  }

  
}
