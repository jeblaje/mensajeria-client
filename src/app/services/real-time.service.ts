import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AppService } from 'src/app/services/app.service';
import Pusher from 'pusher-js';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class RealTimeService {

  pusher: any;
  channel: any;
  channels: any[] = [
    this.orderChannel(),
  ];

  constructor(
    private appService: AppService,
    private http: HttpClient, 
    private router: Router,
  ) {
    // this.channels.forEach(element => {
    //   element;
    // });
   }


  orderChannel() {
    console.log('Real time service');
    // this.pusher = new Pusher(environment.pusher.key, {
    //   cluster: environment.pusher.cluster,
    //   // useTLS: true
    // });
    // this.channel = this.pusher.subscribe('order');
  }



}
