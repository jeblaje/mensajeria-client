import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AppService {

  

  constructor(
    private http: HttpClient
  ) { }


  createRequestOptions(option) {

    let token = localStorage.getItem('token');
    let tokenTipe = localStorage.getItem('token-type');

    if(token == undefined|| tokenTipe == undefined || token == null|| tokenTipe == null ) this.cleaner();
 
    let headersAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `${tokenTipe} ${token}`
    });

    let headersNoAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
    });
    return option === 'auth'? headersAuth  : headersNoAuth;
  }


  private cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }



  
}
