import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { RegisterForm } from '../interfaces/auth-form.interface';
import { CargarBitacora } from '../interfaces/bitacora.interface';

import { Usuario } from '../models/usuario.model';
import { Registro } from '../models/registro.model';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class BitacoraService {

  constructor( private http: HttpClient, 
    private toastr: ToastrService,
    private router: Router,
    private ngZone: NgZone ) {

}


  
  cargarRegistros( desde: number = 1 ) {

    const url = `/api/logs?page=${ desde }`;
    let options = this.createRequestOptions('auth');
    return this.http.get<CargarBitacora>( url, { headers: options } )
            .pipe(
              map( (resp) => {
                  const registros = resp.data['data']
                  .map( 
                    reg => new Registro(
                        reg.id,
                        reg.log_name,
                        reg.description,
                        reg.subject_type,
                        reg.subject_id,
                        reg.causer_type,
                        reg.causer_id,
                        reg.created_at,
                        reg.updated_at,
                        reg.properties
                      )
                  );
                return {
                  total: resp.data['total'],
                  totalResponse: resp.data['to'],
                  registros,
                };
              })
            );
  }


  
private codeResponse(err) {

  if (err.status == 401 || err.status == 400) {
    this.toastr.error(err.error.message);
  }


  if (err.status == 500) {
    this.toastr.error('Estamos teniendo problemas disculpe');
  }

  if (err.status != 401 && err.status != 500) {
    this.toastr.error('Estamos teniendo problemas disculpe');
  }

}

 
  restaurarRegistro( registro: any ) {
    // console.log(registro);
    // /registros/idRegistro/tabla
    const url = `/api/logs/restore/${ registro.logID }/${ registro.table }/${ registro.id }`;
    let options = this.createRequestOptions('auth');
    return this.http.get( url, { headers: options } );
}




  // PRIVATE FUNCTIOS 
  private createRequestOptions(option) {

    let token = localStorage.getItem('token');
    let tokenTipe = localStorage.getItem('token-type');

    if(token == undefined|| tokenTipe == undefined || token == null|| tokenTipe == null ) this.cleaner();
 
    let headersAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `${tokenTipe} ${token}`
    });

    let headersNoAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
    });
    return option === 'auth'? headersAuth  : headersNoAuth;
  }


  private cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }


}
