import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppService } from './app.service';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  
  public total: number = 0; 


  constructor(
    private http: HttpClient, 
    private router: Router,
    private appService: AppService,
  ) {
    this.cargarUsuarios();
   }


  

  cargarUsuarios( desde: number = 1 ) {
    const url = `/api/users?page=${ desde }`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get( url, { headers: options } )
            .pipe(
              map( (resp: any) => {
                console.log(resp);
                this.total = resp.total;
                return resp;
              })
            );
  }


  cargarPedidos( desde: number = 1 ) {
    const url = `/api/users?page=${ desde }`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get( url, { headers: options } )
            .pipe(
              map( (resp: any) => {
                console.log(resp);
                this.total = resp.total;
                return resp;
              })
            );
  }

  // obtenerMedicoPorId( id: string ) {

  //   const url = `${ base_url }/medicos/${ id }`;
  //   return this.http.get( url, this.headers )
  //             .pipe(
  //               map( (resp: {ok: boolean, medico: Medico }) => resp.medico )
  //             );
  // }

  // crearMedico( medico: { nombre: string, hospital: string } ) {

  //   const url = `${ base_url }/medicos`;
  //   return this.http.post( url, medico, this.headers );
  // }
  
  // actualizarMedico( medico: Medico  ) {

  //   const url = `${ base_url }/medicos/${ medico._id }`;
  //   return this.http.put( url, medico, this.headers );
  // }

  // borrarMedico( _id: string ) {

  //   const url = `${ base_url }/medicos/${ _id }`;
  //   return this.http.delete( url, this.headers );
  // }


}
