import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppService } from './app.service';
import { map } from 'rxjs/operators';
import { CargarPedidos } from '../interfaces/order.interface';
import { Order } from '../models/pedido.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(
    private appService: AppService,
    private http: HttpClient, 
  ) { }

  guardarPedido(data) {
    console.log({PedidoService: data});
    const url = `/api/orders`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.post(url, data, { headers: options })
    // return this.http.post(url, data, { headers: options })
      .pipe(
        map((resp: any) => {
          // let asd = resp.resp;
          // console.log({ service: 'Respuesta servidor', resp })
          return resp;
        })
      );
  }

  cargarPedidos( desde: number = 1 ) {

      const url = `/api/orders?page=${ desde }`;
      let options = this.appService.createRequestOptions('auth');
      return this.http.get<CargarPedidos>( url, { headers: options } )
              .pipe(
                map( (resp) => {
                  const orders = resp.data['data'].map( 
                    order => new Order(
                      order.round_trip,
                      order.declared_value,
                      order.created_at,
                      order.updated_at,
                      order.id,
                      order.customer,
                      order.size,
                      order.status,
                      )  
                  );
                  return {
                    total: resp.data['total'],
                    totalResponse: resp.data['to'],
                    orders,
                  };
                })
              );
  }


  cargarServicios() {

    const url = `/api/services`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get<any>( url, { headers: options } );
            
  }
}
