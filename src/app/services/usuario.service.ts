import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { NewUserCreate } from '../interfaces/usuarios-forms.interface';
import { CargarUsuarios } from '../interfaces/cargar-usuarios.interface';

import { Usuario } from '../models/usuario.model';

const base_url = environment.base_url;

declare const gapi: any;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  public auth2: any;
  public usuario: Usuario;

  constructor( private http: HttpClient, 
                private router: Router,
                private ngZone: NgZone ) {

    this.googleInit();
  }

  get token(): string {
    return localStorage.getItem('token') || '';
  }

  // get role(): 'ADMIN_ROLE' | 'CLIENT_ROLE' | 'DELIVERY_ROL' {
  //   // return this.usuario.rol;
  // }

  get id():string {
    return this.usuario.id || '';
  }

  get headers() {
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  createRequestOptions(option) {

    let token = localStorage.getItem('token');
    let tokenTipe = localStorage.getItem('token-type');

    if(token == undefined|| tokenTipe == undefined || token == null|| tokenTipe == null ) this.cleaner();
 
    let headersAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `${tokenTipe} ${token}`
    });

    let headersNoAuth = new HttpHeaders({
        "Accept": "application/json",
        "Content-Type": "application/json",
    });
    return option === 'auth'? headersAuth  : headersNoAuth;
  }


  cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }

  googleInit() {

    return new Promise( resolve => {
      gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: '1045072534136-oqkjcjvo449uls0bttgvl3aejelh22f5.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
        });

        resolve(this.auth2);
      });
    })

  }

  guardarLocalStorage( token: string, menu: any ) {

    localStorage.setItem('token', token );
    localStorage.setItem('menu', JSON.stringify(menu) );

  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('menu');

    this.auth2.signOut().then(() => {

      this.ngZone.run(() => {
        this.router.navigateByUrl('/login');
      })
    });

  }

  validarToken(): Observable<boolean> {
    
    return this.http.get(`${ base_url }/login/renew`, {
      headers: {
        'x-token': this.token
      }
    }).pipe(
      map( (resp: any) => {
        const { email, google, nombre, role, img = '', uid } = resp.usuario;
        this.usuario = new Usuario( nombre, email, '', img, google, role, uid );
        
        this.guardarLocalStorage( resp.token, resp.menu );

        return true;
      }),
      catchError( error => of(false) )
    );

  }


  crearUsuario( formData: NewUserCreate  ) {
   
    const url = `/api/users`;
    let options = this.createRequestOptions('auth');
    return this.http.post(url, formData, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      })
    );

  }

  actualizarUsuario(form: any) {
    console.log({FormService: form});
    const url = `/api/users/${form.id}`;
    let options = this.createRequestOptions('auth');
    return this.http.put(url, form, { headers: options })
    .pipe(
      map( (resp: any) => {
        return resp;
      })
    );
  }

  actualizarPerfil( data: { email: string, nombre: string, role: string } ) {

    data = {
      ...data,
      // role: this.usuario.rol
    }

    return this.http.put(`${ base_url }/users/${ this.id }`, data, this.headers );

  }


  loginGoogle( token ) {
    
    return this.http.post(`${ base_url }/login/google`, { token } )
                .pipe(
                  tap( (resp: any) => {
                    this.guardarLocalStorage( resp.token, resp.menu );
                  })
                );

  }

  
  cargarUsuarios( desde: number = 1 ) {

    const url = `/api/users?page=${ desde }`;
    let options = this.createRequestOptions('auth');
    return this.http.get<CargarUsuarios>( url, { headers: options } )
            .pipe(
              map( (resp) => {
                console.log(resp);
                const usuarios = resp.data['data'].map( 
                  user => new Usuario(user.name, user.email, user.state, user.roles, user.created_at, user.updated_at, user.username, user.document, user.phone, '', user.id )  
                );
                return {
                  total: resp.data['total'],
                  totalResponse: resp.data['to'],
                  usuarios,
                };
              })
            );
  }

  cargarUsuario( id: number) {

    const url = `/api/users/${id}`;
    let options = this.createRequestOptions('auth');
    return this.http.get( url, { headers: options } )
            .pipe(
              map( (resp: any) => {
                // const usuario = resp.usuarios['data'].map( 
                  // );
                // let user = new Usuario(resp.data.nombre_completo, resp.data.correo, resp.data.estado, resp.data.created_at, resp.data.updated_at, resp.data.username, resp.data.cedula, resp.data.telefono, '', resp.data.img, resp.data.rol, resp.data.id )  
                return resp;
                // {
                //   total: resp.usuarios['total'],
                //   totalResponse: resp.usuarios['to'],
                //   usuarios,
                // };
              })
            );
        
  }




  eliminarUsuario( usuario: Usuario ) {
    
      // /usuarios/5eff3c5054f5efec174e9c84
      const url = `/api/users/${ usuario.id }`;
      let options = this.createRequestOptions('auth');
      return this.http.delete( url, { headers: options } );
  }

  guardarUsuario( usuario: Usuario ) {

    return this.http.put(`${ base_url }/usuarios/${ usuario.id }`, usuario, this.headers );

  }



}
