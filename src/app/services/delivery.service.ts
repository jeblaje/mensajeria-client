import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CargarPedidos } from '../interfaces/order.interface';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {

  constructor(
    private appService: AppService,
    private http: HttpClient, 
    private router: Router,
  ) { }


  cargarKits( kits ) {

    const url = `/api/kits/delivery`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.post<any>( url, kits, { headers: options } );
  }

  envioEquipamiento( equipo ) {

    const url = `/api/kits/equipamiento`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.post<any>( url, equipo, { headers: options } );
  }

  deliveryOnService() {
    console.log('connect');
      const url = `/api/deliveries/connect`;
      let options = this.appService.createRequestOptions('auth');
      return this.http.get<any>( url, { headers: options } );
  }

  deliveryOutService() {
    console.log('delete');
    const url = `/api/deliveries/disconnect`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.delete<any>( url, { headers: options } );
  }


  cargarRepartidores() {

    const url = `/api/deliveries`;
    let options = this.appService.createRequestOptions('auth');
    return this.http.get( url, { headers: options } );
  }

  
}
