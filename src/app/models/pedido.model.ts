import { environment } from '../../environments/environment';

const base_url = environment.base_url;

export class Order {

    constructor(
        public round_trip: number | boolean,
        public declared_value: number,
        public created_at: string,
        public updated_at: string,
        public id?: string,
        public customer?: any,
        public size?: any,
        public status?: any,
        

    ) {}

    // get imagenUrl() {

    //     if ( !this.img ) {
    //         return `${ base_url }/upload/usuarios/no-image`;
    //     } else if ( this.img.includes('https') ) {
    //         return this.img;
    //     } else if ( this.img ) {
    //         return `${ base_url }/upload/usuarios/${ this.img }`;
    //     } else {
    //         return `${ base_url }/upload/usuarios/no-image`;
    //     }
    // }
}

