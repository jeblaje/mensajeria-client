import { environment } from '../../environments/environment';

const base_url = environment.base_url;

export class Registro {

    constructor(
      
        public id: number,
        public log_name: string,
        public description: string,
        public subject_type: string,
        public subject_id: number,
        public causer_type: string,
        public causer_id: number,
        public created_at: string,
        public updated_at: string,
        public properties?: [],
    ) {}

}

