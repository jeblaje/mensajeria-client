import { environment } from '../../environments/environment';

const base_url = environment.base_url;

export class Usuario {

    constructor(
        public name: string,
        public email: string,
        public state: string,
        public roles: any,
        public created_at: string,
        public updated_at: string,
        public username?: string,
        public document?: string,
        public phone?: string,
        public password?: string,
        // public rol?: 'ADMIN_ROLE' | 'CLIENT_ROLE' | 'DELIVERY_ROL',
        public id?: string,
        // public created_at?: boolean,
        // public created_at?: boolean,
    ) {}

    // get imagenUrl() {

    //     if ( !this.img ) {
    //         return `${ base_url }/upload/usuarios/no-image`;
    //     } else if ( this.img.includes('https') ) {
    //         return this.img;
    //     } else if ( this.img ) {
    //         return `${ base_url }/upload/usuarios/${ this.img }`;
    //     } else {
    //         return `${ base_url }/upload/usuarios/no-image`;
    //     }
    // }
}

