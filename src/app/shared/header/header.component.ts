import { Component, OnDestroy } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../models/usuario.model';
import { Router } from '@angular/router';
import { ModalDomicilioService } from 'src/app/components/services/modal-domicilio.service';
import { ModalImagenService } from 'src/app/services/modal-imagen.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [
  ]
})
export class HeaderComponent  implements OnDestroy  {

  public usuario: Usuario;
  public domicilio: any = 0;
  public domicilios: any[] = [];
  public domisilioSub: Subscription;

  constructor( 
    private usuarioService: UsuarioService,
    public modalDomicilioService: ModalDomicilioService,
    private router: Router 
  ) {
    this.cargarMisDomicilios();
    this.usuario = usuarioService.usuario;
    this.domicilio = localStorage.getItem('domi') || 'No hay direcciones';
    this.domisilioSub = this.modalDomicilioService.enviarDomi.subscribe( () => this.cargarMisDomicilios());
  }

  
  ngOnDestroy() {
    this.domisilioSub.unsubscribe();
  }

  logout() {
    this.usuarioService.logout();
  }

  buscar( termino: string ) {

    if ( termino.length === 0  ) {
      return;
    }

    this.router.navigateByUrl(`/dashboard/buscar/${ termino }`);
  }


  cambiarDomicilio(value: string) {
    localStorage.setItem('domi', value);
  }

  cargarMisDomicilios(){
    this.modalDomicilioService.cargarMisDomicilios()
      .subscribe( (resp: any) => this.domicilios = resp.addresses);
  }

  
  abrirModal() {
    this.modalDomicilioService.abrirModal('nuevo');
    localStorage.getItem('user');
  }

}
