import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { UsuarioService } from '../../services/usuario.service';

import { Usuario } from '../../models/usuario.model';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent implements OnInit {

  public usuario: Usuario;

  constructor(
    public sidebarService: SidebarService,
    private toastr: ToastrService,
    private router: Router,
    public authService: AuthService,
    private usuarioService: UsuarioService
  ) {
    this.usuario = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit(): void {
  }

  cerrarSesion() {
    let user = JSON.parse(localStorage.getItem('user'));

    this.authService.logout(user).subscribe(resp => {
      this.cleaner();
      this.toastr.success(resp.message);
      this.router.navigate(['/login']);
    },
      (err) => this.errors(err));
  }



  private errors(err) {

    if (err.status == 500) {
      let errorDuplicate = err.error.message.indexOf("SQLSTATE[23000]");
      if (errorDuplicate == 0) {
        this.toastr.error("El correo ya esta registrado!");
        return;
      }
      this.toastr.error("Intenta mas tarde al parecer tenemos problemas!");
      return;
    }

    if (err.status == 404) {
      this.toastr.error("Estamos teniendo problemas de acceso, intenta mas tarde!");
    }

    if (err.status == 403) {
      this.toastr.error(err.error.message);
    }

    this.toastr.error("Error desconocido!");
  }



  private cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }

}
