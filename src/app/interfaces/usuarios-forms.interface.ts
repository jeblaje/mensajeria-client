export interface NewUserForm {
    nombre: string;
    correo: string;
    telefono: string;
    cedula: string;
    password: string;
    rol: string;
    username: string;
    estado: string;
}


export interface NewUserCreate {
    form: NewUserForm;
    idUserMod: string;
    rolUserTemp: string;
}