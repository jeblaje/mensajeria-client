import { Usuario } from '../models/usuario.model';

// export interface CargarUsuario {
//     total: number;
//     totalResponse: number;
//     usuarios: Usuario[];
// }

export interface CargarUsuarios {
    data: UsuarioPaginate[];
    message: string;
}

export interface UsuarioPaginate {
    data: Usuario[];
            current_page: number;
            to: number;
            total: number;
}

// {
//     "current_page": 1,
//     "data": [
//         {
//             "id": 1,
//             "nombre_completo": "Raleigh Koepp",
//             "username": "8773498836",
//             "correo": "reynolds.landen@example.net",
//             "cedula": "8773498836",
//             "telefono": "323-490-0162",
//             "rol": "ADMIN_ROL",
//             "password": "$2y$10$FBiCXv1rpLE9qR.fdi3skOF2lKzFNsEUnGTK587pgH7U7G4tq4sBi",
//             "estado": "Activo",
//             "created_at": "2021-05-12T15:17:34.000000Z",
//             "updated_at": "2021-05-12T15:17:34.000000Z"
//         },
//         {
//             "id": 2,
//             "nombre_completo": "Letitia Turcotte DVM",
//             "username": "5243727053",
//             "correo": "maryjane73@example.com",
//             "cedula": "5243727053",
//             "telefono": "+18145277009",
//             "rol": "DELIVERY_ROL",
//             "password": "$2y$10$DYtDg.5N6vke.hSbeSfqMehHr0wIK4Nefr.eRMbHVogxn4KM/1Iue",
//             "estado": "Activo",
//             "created_at": "2021-05-12T15:17:34.000000Z",
//             "updated_at": "2021-05-12T15:17:34.000000Z"
//         }
//     ],
//     "first_page_url": "http://localhost:8000/api/usuarios?page=1",
//     "from": 1,
//     "last_page": 5,
//     "last_page_url": "http://localhost:8000/api/usuarios?page=5",
//     "links": [
//         {
//             "url": null,
//             "label": "&laquo; Previous",
//             "active": false
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=1",
//             "label": "1",
//             "active": true
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=2",
//             "label": "2",
//             "active": false
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=3",
//             "label": "3",
//             "active": false
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=4",
//             "label": "4",
//             "active": false
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=5",
//             "label": "5",
//             "active": false
//         },
//         {
//             "url": "http://localhost:8000/api/usuarios?page=2",
//             "label": "Next &raquo;",
//             "active": false
//         }
//     ],
//     "next_page_url": "http://localhost:8000/api/usuarios?page=2",
//     "path": "http://localhost:8000/api/usuarios",
//     "per_page": "2",
//     "prev_page_url": null,
//     "to": 2,
//     "total": 10
// },