import { Order } from '../models/pedido.model';

export interface CargarPedidos {
    data: OrderPaginate[];
    message: string;
}

export interface OrderPaginate {
    data: Order[];
            current_page: number;
            to: number;
            total: number;
}
