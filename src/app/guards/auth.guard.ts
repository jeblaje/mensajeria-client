// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, Route } from '@angular/router';
// import { UsuarioService } from '../services/usuario.service';
// import { tap } from 'rxjs/operators';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuard implements CanActivate, CanLoad {

//   constructor( private usuarioService: UsuarioService,
//                private router: Router) {}

//   canLoad(route: Route, segments: import("@angular/router").UrlSegment[]): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
//     return this.usuarioService.validarToken()
//         .pipe(
//           tap( estaAutenticado =>  {
//             if ( !estaAutenticado ) {
//               this.router.navigateByUrl('/login');
//             }
//           })
//         );
//   }

//   canActivate(
//     next: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot) {

//       return this.usuarioService.validarToken()
//         .pipe(
//           tap( estaAutenticado =>  {
//             if ( !estaAutenticado ) {
//               this.router.navigateByUrl('/login');
//             }
//           })
//         );
//   }
  
// }





import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( 
    private authService: AuthService,
    private router: Router
    ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      let tokenn = '';
      const token = this.authService.validarToken()
            .subscribe(resp => {
              // console.log(resp);
              // if (resp == 1) this.router.navigateByUrl('/inicio');
            }, (err) => {
              console.warn(err.statusText);
              if (err.statusText == 'Unauthorized') {
                  localStorage.removeItem('token');
                  localStorage.removeItem('token-type');
                  localStorage.removeItem('menu');
                  localStorage.removeItem('user');
                  localStorage.removeItem('idLog');
                  this.router.navigateByUrl('/login')
              };
            });
      // const url = route['_routerState'].url;
      
      // console.log({token, url});

      // if(url === '/login') {
      //     // if (token) {
      //     this.router.navigateByUrl('/login');
      //   // }
      // }


      // if(url != '/login') {
      //   if (!token) {
      //   this.router.navigateByUrl('/login');
      // }

    // }

    return true;

  }
  
}