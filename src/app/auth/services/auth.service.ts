import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ToastrService } from 'ngx-toastr';
// import { Login, LoginRequest } from '../interfaces/auth.interface';
// import { User } from 'src/app/pages/models/usuario.model';
// import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    // private toastr: ToastrService
  ) { }

  createRequestOptions(option) {

    let token = localStorage.getItem('token');
    let tokenTipe = localStorage.getItem('token-type');

    if (token == undefined || tokenTipe == undefined || token == null || tokenTipe == null) this.cleaner();

    let headersAuth = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `${tokenTipe} ${token}`
    });

    let headersNoAuth = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
    });
    return option === 'auth' ? headersAuth : headersNoAuth;
  }

  cleaner() {
    localStorage.removeItem('token');
    localStorage.removeItem('token-type');
    localStorage.removeItem('menu');
    localStorage.removeItem('user');
    localStorage.removeItem('idLog');
  }


  validarToken() {
    const url = `/api/validateToken`;
    let options = this.createRequestOptions('auth');
    return this.http.get(url, { headers: options });
    // .pipe(
    //   map( (resp: any) => {
    //     console.log(resp);
    //     return true;
    //   }),
    //   catchError( error => of(false) )
    // );
  }


  // USE ['auth'] FOR PETITIONS REQUIRING AUTH
  // USE ['no-auth'] FOR PETITIONS NO REQUIRING AUTHs

  login(user: any) {

    const url = `/api/login`;
    let options = this.createRequestOptions('no-auth');
    return this.http.post(url, user, { headers: options })
      .pipe(
        map((resp: any) => {
          return resp;
        })
      );
  }


  logout(user) {

    const url = `/api/logout`;
    let options = this.createRequestOptions('auth');
    return this.http.get(url, { headers: options })
      .pipe(
        map((resp: any) => { this.cleaner(); return resp; })
      );

  }

  register(user) {
    let formatData = {
      name: user.name,
      email: user.email,
      phone: user.phone,
      password: user.pass1,
    };


    const url = `/api/register`;
    let options = this.createRequestOptions('no-auth');
    return this.http.post(url, formatData, { headers: options })
      .pipe(
        map((resp: any) => {
          return resp;
        })
      );
  }

  menu(rol) {
    // let iconClassConst: string = 'nav-icon fas fa-';
    let rolTemp = rol != [] ? rol[0].id : 3;
    let menu: any[] = [


      // INICIO
      // {
      //   titulo: 'Inicio',
      //   icono: 'mdi mdi-apps',
      //   url: 'dash-cli',
      //   submenu: []
      // },


      // PEDIDOS
      {
        titulo: 'Pedidos',
        icono: 'mdi mdi-folder-lock-open',
        submenu: [
          { titulo: 'Pedidos', url: 'pedidos' },
          { titulo: 'Nuevo pedido', url: 'pedidos/gestion/nuevo' },
          // { titulo: 'Hospitales', url: 'hospitales' },
          // { titulo: 'Médicos', url: 'medicos' },
        ]
      },


    ];


    if (rolTemp == 3) {
      
      // INICIO
      menu.unshift(
        {
          titulo: 'Inicio',
          icono: 'mdi mdi-apps',
          url: 'dash-cli',
          submenu: []
        },
      );
    }


    if (rolTemp == 1) {
      
      // MANTENIMIENTOS
      menu.unshift(
        {
          titulo: 'Inicio',
          icono: 'mdi mdi-apps',
          url: 'dash-adm',
          submenu: []
        },
      );

      menu.push(
        {
          titulo: 'Mantenimientos',
          icono: 'mdi mdi-folder-lock-open',
          submenu: [
            { titulo: 'Roles', url: 'admin/roles' },
            { titulo: 'Usuarios', url: 'admin' },
            { titulo: 'Registros', url: 'admin/logs' },
            // NUEVOS REGISTROS
            { titulo: 'Nuevo usuario', url: 'admin/usuarios/gestion/nuevo' },
            // { titulo: 'Nuevo aliado', url: 'admin/aliados/gestion/nuevo' },
            // { titulo: 'Nueva mensajeria', url: 'admin/mensajerias/gestion/nuevo' },
            // { titulo: 'Nueva establecimiento', url: 'admin/establecimientos/gestion/nuevo' },
          ]
        },

      );
    }


    if (rolTemp == 2) {
     
      menu.unshift(
        {
          titulo: 'Inicio',
          icono: 'mdi mdi-apps',
          url: 'dash-dev',
          submenu: []
        },
      );

      menu.push(
        {
          titulo: 'Detalles',
          icono: 'mdi mdi-folder-lock-open',
          submenu: [
            { titulo: 'Pedidos', url: 'repartidores/usuarios' },
            { titulo: 'Registros', url: 'repartidores/registros' },
          ]
        },

      );


    }



    localStorage.setItem('menu', JSON.stringify(menu));

  }



  validEmail(email) {
    const url = `/api/validEmail`;
    let options = this.createRequestOptions('no-auth');
    return this.http.post(url, email, { headers: options })
      .pipe(
        map((resp: any) => {
          let asd = resp.resp;
        })
      );
  }


}
