import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import Swal from 'sweetalert2'

import { UsuarioService } from '../../services/usuario.service';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  public formSubmitted = false;

  public registerForm = this.fb.group({
    name: ['Fernando', Validators.required],
    email: ['test100@gmail.com', [Validators.required, Validators.email]],
    phone: ['3012083737', Validators.required],
    pass1: ['123456', Validators.required],
    pass2: ['123456', Validators.required],
    terminos: [true, Validators.required],
  }, {
    validators: this.passwordsIguales('pass1', 'pass2')
  });

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  crearUsuario() {
    this.formSubmitted = true;
    console.log(this.registerForm.value);

    if (this.registerForm.invalid) {
      return;
    }
    this.authService.register(this.registerForm.value)
      .subscribe(resp => {
        console.log({Responsebackend: resp})
        this.toastr.success(resp.message);
        this.router.navigate(['/login']);
      }, (err) => this.errors(err));


  }


  private errors(err) {
    console.warn(err);
    if (err.status == 500) {
      let errorDuplicate = err.error.message.indexOf("SQLSTATE[23000]");
      if (errorDuplicate == 0) {
        this.toastr.error("El correo ya esta registrado!");
        return;
      }
      this.toastr.error("Intenta mas tarde al parecer tenemos problemas!");
      return;
    }

    if (err.status == 400) {
      this.toastr.error(err.error.message);
    }

    this.toastr.error("Error desconocido!");
  }


  campoNoValido(campo: string): boolean {

    if (this.registerForm.get(campo).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }

  }

  contrasenasNoValidas() {
    const pass1 = this.registerForm.get('pass1').value;
    const pass2 = this.registerForm.get('pass2').value;

    if ((pass1 !== pass2) && this.formSubmitted) {
      return true;
    } else {
      return false;
    }

  }

  aceptaTerminos() {
    return !this.registerForm.get('terminos').value && this.formSubmitted;
  }

  passwordsIguales(pass1Name: string, pass2Name: string) {

    return (formGroup: FormGroup) => {

      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null)
      } else {
        pass2Control.setErrors({ noEsIgual: true })
      }


    }
  }

}
